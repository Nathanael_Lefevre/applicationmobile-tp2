package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    private MutableLiveData<Book> selectedBook = new MutableLiveData<>();
    private LiveData<List<Book>> allBooks;

    private BookDao bookDao;

    public BookRepository(Application application) {
        BookRoomDatabase db = BookRoomDatabase.getDatabase(application);
        bookDao = db.bookDao();
        allBooks = bookDao.getAllBooks();
    }


    public void updateBook(Book Book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.updateBook(Book);
        });
    }


    public void insertBook(Book newBook) {
        databaseWriteExecutor.execute(() -> {
            bookDao.insertBook(newBook);
        });
    }


    public MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }


    public void deleteBook(Book book) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deleteBook(book.getId());
        });
    }


    public void refreshSelectedBook(long id)
    {
        Future<Book> fbook = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);

        });
        try {
            selectedBook.setValue(fbook.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }


    }


    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }

}