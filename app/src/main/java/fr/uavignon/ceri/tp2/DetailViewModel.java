package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<Book> bookToDisplay;

    public MutableLiveData<Book> getBookToDisplay() {
        return bookToDisplay;
    }

    public void setBookToDisplay(long id) {
        repository.refreshSelectedBook(id);
        bookToDisplay = repository.getSelectedBook();
    }

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        bookToDisplay = repository.getSelectedBook();
    }

    public String insertOrUpdateBook(String titre, String auteur, String pubYear, String genre, String editeur, int id) throws Exception{
        if(titre.equals("") || auteur.equals("")){
            throw new Exception();
        }
        if ((id != -1) && (bookToDisplay != null)){
            bookToDisplay.getValue().setTitle(titre);
            bookToDisplay.getValue().setAuthors(auteur);
            bookToDisplay.getValue().setYear(pubYear);
            bookToDisplay.getValue().setGenres(genre);
            bookToDisplay.getValue().setPublisher(editeur);

            repository.updateBook(bookToDisplay.getValue());

            return "Le livre \"" + titre + "\" a été mis à jour";
        }else{
            Book newBook = new Book(titre, auteur, pubYear, genre, editeur);

            repository.insertBook(newBook);
            return "Le livre \"" + titre + "\" a été ajouté";
        }
    }
}
