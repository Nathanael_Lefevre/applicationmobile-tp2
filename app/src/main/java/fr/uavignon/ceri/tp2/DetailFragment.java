package fr.uavignon.ceri.tp2;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    private DetailViewModel viewModel;
    private Button buttonUpdate;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel.setBookToDisplay((int)args.getBookNum());

        //Book book = Book.books[(int)args.getBookNum()];

        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        buttonUpdate = (Button) view.findViewById(R.id.buttonUpdate);

        /*
        textTitle.setText(book.getTitle());
        textAuthors.setText(book.getAuthors());
        textYear.setText(book.getYear());
        textGenres.setText(book.getGenres());
        textPublisher.setText(book.getPublisher());
        */

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        observerSetup();
        listenerSetup();
    }

    private void observerSetup() {
        viewModel.getBookToDisplay().observe(getViewLifecycleOwner(),
                new Observer<Book>() {
                    @Override
                    public void onChanged(Book book) {
                        if(book != null){
                            textTitle.setText(book.getTitle());
                            textAuthors.setText(book.getAuthors());
                            textYear.setText(book.getYear());
                            textGenres.setText(book.getGenres());
                            textPublisher.setText(book.getPublisher());
                        }
                    }
                });

    }

    private void listenerSetup() {
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String titre = textTitle.getText().toString();
                String auteur = textAuthors.getText().toString();
                String pubYear = textYear.getText().toString();
                String genre = textGenres.getText().toString();
                String editeur = textPublisher.getText().toString();

                DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
                int id = (int)args.getBookNum();

                try {
                    String msg = viewModel.insertOrUpdateBook(titre, auteur, pubYear, genre, editeur, id);
                    Snackbar.make(view, msg, Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }catch (Exception e){
                    Snackbar.make(view, Html.fromHtml("<font color=\"#ff0000\">Veuillez préciser le titre et les auteurs</font>"), Snackbar.LENGTH_LONG).show();
                    //Snackbar.make(view, "Veuillez préciser le titre et les auteurs", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                }
            }
        });
    }
}